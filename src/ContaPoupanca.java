
public class ContaPoupanca extends ContaBancaria{
	
	public ContaPoupanca(){
		super();
	}
	
	private double rendimento;

	public double getRendimento() {
		return rendimento;
	}

	public void setRendimento(double rendimento) {
		this.rendimento = rendimento;
	}
	
	

}
