import java.util.ArrayList;


public class ControleContaBancaria {
	
	private ArrayList<ContaBancaria> listaContas;
	
	public ControleContaBancaria(){
		listaContas = new ArrayList<ContaBancaria>();
	}
	
	public String adicionarContaCorrente(ContaBancaria umaContaCorrente){
		String mensagem = "Conta Corrente Adicionada com Sucesso!";
		listaContas.add(umaContaCorrente);
		return mensagem;
	}
	
	public String adicionarContaPoupanca(ContaPoupanca umaContaPoupanca){
		String mensagem = "Conta Poupanca Adicionada com Sucesso!";
		listaContas.add(umaContaPoupanca);
		return mensagem;
	}
	
    public ContaBancaria Pesquisa(String umNumero){
        for (ContaBancaria umaConta: listaContas) {
            if (umaConta.getConta().equalsIgnoreCase(umNumero)) return umaConta;
        }
        return null;
    }

}
